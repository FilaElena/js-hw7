// //Калькулятор

// let Calc = function () {
//     this.turnOffOn = function(status) {
//         if (status === 'Off') {
//             alert ('Включите калькулятор');
//         } else {
//             this.get ();
//         }
//     }

//     this.get = function() {
//         this.a = +prompt ('Введите первое число');
//         this.b = +prompt ('Введите второе число');
//         this.oper = prompt ('Введите операцию: +, -, *, /');

//         this.operation();
//     };

//     this.operation = function () {
//         switch(this.oper) {
//             case '+':
//                 this.result = this.a + this.b;
//             break;
//             case '-':
//                 this.result = this.a - this.b;
//             break;
//             case '*':
//                 this.result = this.a * this.b;
//             break;
//             case '/':
//                 this.result = this.a / this.b;
//             break;
//             default: this.result = 0;
//         }
//         this.show ();
//     }

//     this.show = function () {
//         document.write(`${this.a} ${this.oper} ${this.b} = ${this.result} `);
//     }
// }


// let simpleCalc = new Calc ();
// simpleCalc.turnOffOn('On');


//Автомобиль

let Auto = function () {
    this.startCar = function(status) {
        if (status === 'Off') {
            alert ('Заведите машину');
        } else {
            this.inputInfo ();
        }
    }

    this.inputInfo = function () {
        this.brand = prompt ('Введите марку машины');
        this.number = prompt ('Введите номер машины');
        this.drive ();
    }

    this.drive = function () {
        this.diraction = prompt ('Какая передача включена? (вперед/назад/нейтральная)');
        if (this.diraction === 'нейтральная') {
            alert ('Включена нейтральная передача. Начните движение!');
            this.drive ();
        } else {
            this.speed = +prompt ('Введите скорость движение (км/ч)');
        }
    }

    this.calcDistance = function (time) {
        switch(this.diraction) {
            case 'вперед':
                this.result = time/60 * this.speed;
                this.resultText = `вперед ${this.result.toFixed(2)}`;
            break;
            case 'назад':
                this.result = time/60 * this.speed;
                if (this.result > 0.3) {
                    alert ('Не советуем так долго ездить задним ходом!');
                }
                this.resultText = `задним ходом ${this.result.toFixed(2)}`;
            break;
            default: this.result = 0;
        }
        this.showResult ();
    }

    this.showResult = function () {
        if (this.brand === undefined || this.result === 0) {
            document.write(`Вы стояли на месте. Проверьте введенные данные.`);
        } else {
            document.write(` У вас автомобиль: ${this.brand} ${this.number}. Вы проехали ${this.resultText} километров.`);
        }
    }

}

let myAuto = new Auto ();
myAuto.startCar ('On');
myAuto.calcDistance (20);
